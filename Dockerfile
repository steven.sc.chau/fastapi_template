FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
	python3-pip python3-dev build-essential cmake pkg-config libx11-dev libatlas-base-dev libgtk-3-dev libboost-python-dev vim \
        && cd /usr/local/bin \
        && ln -s /usr/bin/python3 python \
        && pip3 install --upgrade pip

COPY . /app

WORKDIR /app

RUN python -m pip install -r requirements.txt

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV CB_HOST=http://104.215.197.138 \
    CB_USERNAME=teamone \
    CB_PASSWORD=FoodHotHouse@2020 \
    CB_BUCKETNAME=teamone_data

EXPOSE 5042

CMD ["uvicorn", "app:api", "--host", "0.0.0.0", "--port", "5042"]