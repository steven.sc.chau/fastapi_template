## API template

<a href='http://aicoedevops.southeastasia.cloudapp.azure.com/jenkins/job/Pipeline-FastApiTemplate/lastBuild/console'>
    <img id='imgBuild' src='http://aicoedevops.southeastasia.cloudapp.azure.com/jenkins/buildStatus/icon?job=Pipeline-FastApiTemplate&config=BuildBadge' onload='this.src=this.data-canonical-src' alt='Build Status' />
</a>
<a href='http://aicoedevops.southeastasia.cloudapp.azure.com/jenkins/job/Pipeline-FastApiTemplate/allure/'>
    <img id='imgTest' src='http://aicoedevops.southeastasia.cloudapp.azure.com/jenkins/buildStatus/icon?job=Pipeline-FastApiTemplate&config=TestBadge' onload='this.src=this.data-canonical-src' alt='Test Status' />
</a>
<a href='http://aicoedevops.southeastasia.cloudapp.azure.com/jenkins/job/Pipeline-FastApiTemplate/lastBuild/console'>
    <img id='imgDeploy' src='http://aicoedevops.southeastasia.cloudapp.azure.com/jenkins/buildStatus/icon?job=Pipeline-FastApiTemplate&config=DeployBadge' onload='this.src=this.data-canonical-src' alt='Deploy Status' />
</a>

This is an example repo of a how to serve ML model using fastapi in python 3.6+ environment

Api url:  
https://fastapitemplate.azurewebsites.net  

A few things to do with the project:
* install softwares: ```make install```
* test code: ```make test```
* lint code: ```make lint```
* build docker image: ```make build```
* to build you docker image after all the tests are passed: ```make all```

To configure the couchbase setting, change the "ENV" line in the Dockerfile as per your team's config.

Example:
Couchbase Settings for Team One will be as follows:
```
ENV CB_HOST=http://104.215.197.138 \
    CB_USERNAME=teamone \
    CB_PASSWORD=FoodHotHouse@2020 \
    CB_BUCKETNAME=teamone_data
```

Please configure the deploy.properties for CI job reading.  

Format:  
```
# docker image
image.name=fastapi_template
image.version=1.0.0

# notification emails, seperate by comma
email.receiver=xxx@prudential.com.sg,yyy@prudential.com.hk

```


Your application by default will be hosted <host_name>:5042

Make with ❤️ by AICoE