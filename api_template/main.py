"""
Main API Serving Library
"""

import logging
from api_template import data_model
from api_template.utilities import check_authentication_header
from api_template.model import inference
from fastapi import FastAPI, Depends, Body
from fastapi.security.api_key import APIKey

app = FastAPI(
        title='api template',
        description='''api template''',
        version='0.0.1'
    )

logger = logging.getLogger("app")

@app.get('/')
def helloword():
    return {'status': 200, 'message': 'hello world'}
    

@app.post('/inference')
def model_inference(data: data_model.DataModel = Body(..., embed=True),
                    api_key: APIKey = Depends(check_authentication_header)):
    result = ""
    result = inference(
                bmi = data.bmi,
                sleepingtime_perday = data.sleepingtime_perday,
                waterLitre_perday = data.waterLitre_perday,
                anyExercise_perweek = data.anyExercise_perweek,
                )
    return {"result": result, "message": "successful", "model": "health_score:0.0.1"}
